import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */
public class DadosCliente implements Serializable {
    
    private ArrayList<Compra> minhas_compras;
    private String cliente;
    
    /**
     * Cria um objecto com o código de cliente e zero compras
     * @param s código de cliente
     */
    public DadosCliente(String s){
        minhas_compras=new ArrayList<>();
        cliente=s;
    }
    
    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe
     * @param par objecto com os valores
     */
    public DadosCliente(DadosCliente par) {
        this.minhas_compras = par.getMinhas_compras();
        this.cliente = par.getCliente();
    }

    /**
     * Devolve o código de cliente
     * @return código de cliente
     */
    public String getCliente() {
        return cliente;
    }

    /**
     * Devolve a lista de compras do cliente
     * @return lista de compras do cliente
     */
    public ArrayList<Compra> getMinhas_compras() {
        ArrayList<Compra> aux= new ArrayList<>();
        for(Compra c:this.minhas_compras){
            aux.add(c.clone());
        }
        return aux;
    }

    /**
     * Altera o código de cliente para o valor enviado por argumento
     * @param cliente novo código de cliente
     */
    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    /**
     * Altera a lista de compras do cliente para o valor enviado por argumento
     * @param minhas_compras nova lista de compras
     */
    public void setMinhas_compras(ArrayList<Compra> minhas_compras) {
        ArrayList<Compra> aux= new ArrayList<>();
        for(Compra c:minhas_compras){
            aux.add(c.clone());
        }
        this.minhas_compras = aux;
    }
    
    /**
     * Adiciona uma compra a lista de compras do cliente
     * @param c compra a adicionar
     */
    public void add_compra(Compra c) {
        minhas_compras.add(c.clone());
    }

    /**
     * Devolve o total de compras mês a mês
     * @return array com o total de compras mês a mês
     */
    public int[] get_compras_mes_a_mes() {
        int[] aux=new int[12];
        init_array(aux,12);
        for(Compra c:minhas_compras){
            int mes=c.getMes();
            aux[mes-1]++;
        }
        return aux;
    }
    
    private void init_array(int[] array, int size){
        int i;
        for(i=0;i<size;i++){
            array[i]=0;
        }
    }
    
    private void init_array(float[] array, int size){
        int i;
        for(i=0;i<size;i++){
            array[i]=0;
        }
    }

    /**
     * Devolve o total de produtos comprados mês a mês
     * @return array com o total de produtos mês a mês
     */
    public int[] get_prod_mes_a_mes() {
        int i=0;
        int[] aux=new int[12];
        init_array(aux,12);
        HashMap<Integer,HashSet<String>> aux2=new HashMap<>();
        init_hash_map(aux2,12);
        for(Compra c:minhas_compras){
            int mes=c.getMes();
            String prod=c.getProduto();
            HashSet<String> aux3=aux2.get(mes-1);
            aux3.add(prod);
            aux2.put(mes-1, aux3);
        }
        for(HashSet<String> s:aux2.values()){
            aux[i]=s.size();
            i++;
        }
        return aux.clone();
    }
    


    private void init_hash_map(HashMap<Integer, HashSet<String>> aux2, int size) {
        int i;
        for(i=0;i<size;i++){
           HashSet<String> aux=new HashSet<>();
           aux2.put(i,aux);
       }
    }

    /**
     * Devolve a faturação mês a mês
     * @return array com a faturação mês a mês
     */
    public float[] get_fat_mes_a_mes() {
       float[] aux=new float[12];
       init_array(aux, 12);
       for(Compra c:minhas_compras){
           int mes=c.getMes();
           aux[mes-1]+=c.getQuantidade()*c.getPreco();
       }
       return aux.clone();
    }
    
    /**
     * Devolve o códigoe o total dos produtos mais comprados por ordem decrescente
     * @return treeset dos produtos mais comprados por ordem decrescente
     */
    public TreeSet<Codigo_e_total> get_all_comp_ord(){
        HashMap<String,Codigo_e_total> res=new HashMap<>();
        for(Compra c:minhas_compras){
            String produto=c.getProduto();
            int quantidade=c.getQuantidade();
            if(res.containsKey(produto)){
                Codigo_e_total aux=res.get(produto);
                float total=aux.getTotal()+quantidade;
                aux.setTotal(total);
                res.put(produto, aux);
            }else{
                Codigo_e_total ct=new Codigo_e_total(produto, quantidade);
                res.put(produto,ct);
            }
        }
        TreeSet<Codigo_e_total> ts = new TreeSet<>(new Codigo_e_total_comparator());
        ts.addAll(res.values());
        return ts;
    }

    /**
     * Devolve o total de que o cliente gastou num produto com o seu código associado
     * @param produto código do produto do qual queremos o total de faturação
     * @return código de cliente com o total de faturação associado
     */
    public Codigo_e_total tot_fat_of_prod(String produto) {
        Codigo_e_total res=new Codigo_e_total(this.getCliente(), 0);
        for(Compra c:minhas_compras){
            if(c.getProduto().equals(produto)){
                float total=res.getTotal()+c.getPreco()*c.getQuantidade();
                res.setTotal(total);
            }
        }
        return res;
    }
    
    /**
     * Devolve o número de produtos diferentes que um cliente comprou
     * @return total de produtos diferentes
     */
    public int tot_dif_comprados(){
        TreeSet<String> aux= new TreeSet<>();
        for (Compra c:minhas_compras){
            aux.add(c.getProduto());
        }
        return aux.size();
    }
  
    /**
     * Verifica se o cliente comprou um determinado produto
     * @param produto código de produto a verificar se o cliente comprou
     * @return true se o cliente tiver comprado, false caso contrário
     */
    public boolean comprei(String produto){
        for(Compra c:minhas_compras){
            if(c.getProduto().equals(produto)){
                return true;
            }
        }
        return false;
    }

    /**
     * Verifica se dois objectos são iguais
     * @param o objecto a qual deve ser comparado
     * @return true se foram iguais, false caso contrário
     */
    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if ((o == null) || (this.getClass() != o.getClass())) {
            return false;
        }
        return false;
    }

    /**
     * Cria um clone deste objecto
     * @return clone do objecto
     */
    public DadosCliente clone() {
        return new DadosCliente(this);
    }
    
    /**
     * Retorna os dados do objecto em formato de texto
     * @return string com a informação do objecto
     */
    public String toString(){
    StringBuilder s = new StringBuilder();
    s.append(this.getCliente());
    s.append(System.lineSeparator());
    for(Compra c:minhas_compras){
        s.append(c.toString());
        s.append(System.lineSeparator());
    }
    return s.toString();
    }
   
    
}
    

