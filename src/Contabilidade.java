import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */
public class Contabilidade implements Serializable {
    
    
    private HashMap<String,DadosProduto> database_produtos;
    private float tot_faturacao_n;
    private float tot_faturacao_p;
    
    /**
     * Construtor de classe com os valores por defeito
     */
    public Contabilidade(){
        database_produtos= new HashMap<>();
        tot_faturacao_n=0;
        tot_faturacao_p=0;
    }
    
    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe 
     * @param c objecto com os valores
     */
    public Contabilidade (Contabilidade c){
        this.tot_faturacao_n=c.getTot_faturacao_n();
        this.tot_faturacao_p=c.getTot_faturacao_p();
        this.database_produtos=c.getDatabaseProdutos();
    }


    /**
     * Devolve um float com o total facturado em compras do tipo n
     * @return total facturado em tipo n
     */
    public float getTot_faturacao_n() {
        return tot_faturacao_n;
    }

    /**
     * Devolve um float com o total facturado em compras do tipo p
     * @return total facturado em tipo p
     */
    public float getTot_faturacao_p() {
        return tot_faturacao_p;
    }
    
    /**
    * Devolve a lista de compras organizadas por produto
    */
    private HashMap<String, DadosProduto> getDatabaseProdutos() {
        HashMap<String, DadosProduto> aux=new HashMap<>();
        for(DadosProduto d:database_produtos.values()){
            aux.put(d.getProduto(),d.clone());
        }
        return aux;
    }

    /**
     * Altera o valor da base de dados de produto para o passado por argumento
     * @param database_produtos novo valor da base de dados de produto
     */
    public void setDatabase_produtos(HashMap<String, DadosProduto> database_produtos) {
        HashMap<String, DadosProduto> aux=new HashMap<>();
        for(DadosProduto d:database_produtos.values()){
            aux.put(d.getProduto(),d.clone());
        }
        this.database_produtos = aux;
    }

    
     /**
     * Altera o total facturado em compras do tipo n para o passado por argumento
     * @param tot_faturacao_n  total facturado em compras N
     */
    public void setTot_faturacao_n(float tot_faturacao_n) {
        this.tot_faturacao_n = tot_faturacao_n;
    }

    /**
     * Altera o total facturado em compras do tipo p para o passado por argumento
     * @param tot_faturacao_p  total facturado em compras P
     */
    public void setTot_faturacao_p(float tot_faturacao_p) {
        this.tot_faturacao_p = tot_faturacao_p;
    }
    
    
    /**
     * Adiciona uma linha às compra 
     * @param line linha a adicionar às compras
     */
    public void add_compra(String line) {
        DadosProduto aux=new DadosProduto(line);
        database_produtos.put(line, aux);
    }

    /**
     * Adiciona uma compra 
     * @param c compra a adicionar
     */
    public void add_compra(Compra c){
        String produto=c.getProduto();
        if(database_produtos.containsKey(produto)){
            DadosProduto aux= database_produtos.get(produto);
            aux.add_compra(c.clone());
            database_produtos.put(produto,aux);
            char tipo=c.getTipo();
            if(tipo=='N'){
                tot_faturacao_n+=c.getQuantidade()*c.getPreco();
            }else{
                tot_faturacao_p+=c.getQuantidade()*c.getPreco();
            }
        }
    }

    /**
     * Devolve um inteiro com o total de produtos nao comprados
     * @return total produtos nao comprados
     */
    public int get_tot_not_comp() {
        int tot=0;
        for(DadosProduto d:database_produtos.values()){
            ArrayList<Compra> a=d.getMinhas_compras();
            if(a.size()==0){
                tot++;
            }
        }
        return tot;
    }

    /**
     * Devolve um float com o total facturado
     * @return total facturado
     */
    public float get_tot_fat() {
        return this.getTot_faturacao_n()+this.getTot_faturacao_p();
    }

    /**
     * Devolve um aray de inteiros com o total de compras por mês
     * @param i numero de meses
     * @return total de compras por mês
     */
    public int[] get_tot_compras_por_mes(int i) {
        int[] aux=new int[i];
        init_array(aux,i);
        for(DadosProduto d:database_produtos.values()){
            ArrayList<Compra> a= d.getMinhas_compras();
            for(Compra c:a){
                int mes=c.getMes();
                aux[mes-1]++;
            }
        }
        return aux.clone();
    }

    /**
     * Inicializa um array de inteiros a 0
     * @param array a ser iniciado
     * @param size tamanho do array
     */
    private void init_array(int[] array, int size){
        int i;
        for(i=0;i<size;i++){
            array[i]=0;
        }
    }
 
    /**
     * Inicializa um array de floats a 0
     * @param array a ser iniciado
     * @param size tamanho do array
     */
    private void init_array(float[] array, int size){
        int i;
        for(i=0;i<size;i++){
            array[i]=0;
        }
    }

    /**
     * Devolve um aray de floats com o total facturado por mês
     * @param i numero de meses
     * @return total de facturação por mês
     */
    public float[] get_tot_faturacao_por_mes(int i) {
        float[] aux=new float[i];
        init_array(aux,i);
        for(DadosProduto d:database_produtos.values()){
            ArrayList<Compra> a= d.getMinhas_compras();
            for(Compra c:a){
                int mes=c.getMes();
                aux[mes-1]+=c.getPreco()*c.getQuantidade();
            }
        }
        return aux.clone();
    }
 
    /**
     * Devolve os produtos que nunca foram comprados e o seu total
     * @return produtos nao comprados e o seu total
     */
    public ParArrayListInt get_not_comp_by_order() {
        int total=0;
        ArrayList<String> aux = new ArrayList<>();
        for(DadosProduto a:database_produtos.values()){
            ArrayList<Compra> k = a.getMinhas_compras();
            if (k.isEmpty()){
                aux.add(a.getProduto());
                total++;
            }
        }
        Collections.sort(aux);
        return new ParArrayListInt(aux,total);
    }
    
     /**
     * Devolve o total de compras e o total facturado mês por mês de um produto
     * @param produto codigo de produto
     * @param i numero de meses
     * @return total de compras e total facturado mês por mês
     */
    public DoisArraysInt_DoisArraysFloats prod_fat_comp_mes_a_mes(String produto, int i){
        int[] totComprasN=new int[i];
        int[] totComprasP=new int[i];
        float[] totFacturadoN=new float[i];
        float[] totFacturadoP=new float[i];
        for (DadosProduto d : database_produtos.values()){
            ArrayList<Compra> a = d.getMinhas_compras();
            if(d.getProduto().equals(produto)){
                for(Compra c:a){
                    int mes=c.getMes();
                    if (c.getTipo()== 'N'){
                        totComprasN[mes-1]++;
                        totFacturadoN[mes-1] += c.getQuantidade()*c.getPreco();
                    }
                    else{
                        totComprasP[mes-1]++;
                        totFacturadoP[mes-1] += c.getQuantidade()*c.getPreco();
                    }
                }
            } 
        }
        
    
    return new DoisArraysInt_DoisArraysFloats(totComprasN, totComprasP, totFacturadoN, totFacturadoP);
    }
    

    /**
     * Devolve os total facturado por mês de um produto
     * @param produto código de produto
     * @param i número de meses
     * @return total facturado por mês de um produto
     */
    public float[] get_tot_faturacao_mes_a_mes(String produto,int i) {
        float[] totFatura=new float[i];
        init_array(totFatura,i);
        for(DadosProduto d:database_produtos.values()){
            ArrayList<Compra> a= d.getMinhas_compras();
            if(d.getProduto().equals(produto)){
                for(Compra c:a){
                    int mes=c.getMes();
                    totFatura[mes-1]+=c.getPreco()*c.getQuantidade();
                }
            }
        }
        return totFatura.clone();
    }

    /**
     * Devolve os total de compras por mês de um produto
     * @param produto código de produto
     * @param i número de meses
     * @return total de compras por mês de um produto
     */
    public int[] get_tot_compras_mes_a_mes(String produto,int i){
        int[]totCompras=new int[i];
        init_array(totCompras,i);
        for(DadosProduto d:database_produtos.values()){
            ArrayList<Compra> a = d.getMinhas_compras();
            if(d.getProduto().equals(produto)){
                for(Compra c:a){
                    int mes=c.getMes();
                    System.out.println(c.getMes());
                    totCompras[mes-1]++;
                }
            }
        }
        return totCompras.clone();
    }

    /**
     * Devolve os codigos dos produtos mais vendidos e total
     * @return total de produtos mais vendidos e total
     */
    public TreeSet<Codigo_e_total> get_most_sold(){
        TreeSet<Codigo_e_total> res = new TreeSet<>(new Codigo_e_total_comparator());
        for (DadosProduto a :database_produtos.values()){
            res.add(new Codigo_e_total(a.getProduto(), a.get_tot_sold()));
        }  
        return res;
    }
    
    
     /**
     * Retorn uma cópia de um cliente
     * @return Cliente com a cópia
     */
    public Contabilidade clone(){
       return new Contabilidade(this);
   }

    /**
     * Verifica se um Cliente é igual a outro Cliente
     * @param o Cliente a qual deve ser comparado
     * @return true se os Clientes foram iguais,false caso contrário
     */
    public boolean equals(Object o) {
       if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        Contabilidade a=(Contabilidade) o;
        return (this.tot_faturacao_n==a.getTot_faturacao_n()  );
    }

    /**
     * Devolve a informação do cliente sobre a forma de texto
     * @return string com a informação de cliente
     */
    public String toString(){
        StringBuilder s = new StringBuilder();
        for(DadosProduto d:database_produtos.values()){
            System.out.println(d.toString());
            System.lineSeparator();
        }
        s.append(this.getTot_faturacao_n());
        s.append("      "); 
        s.append(this.getTot_faturacao_p());
        return s.toString();
    }

    
    
}
