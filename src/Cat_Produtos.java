import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */
public class Cat_Produtos implements Serializable {
    private HashMap<String,Produto> cat_produto;

    /**
     * Construtor de classe com os valores por defeito
     */
    public Cat_Produtos() {
    cat_produto=new HashMap<>();
    }
    
    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe 
     * @param c objecto com os valores
     */
    public Cat_Produtos(Cat_Produtos c){
        cat_produto=c.getCat_produto();
    }
    
    /**
     * Adiciona um produto ao catálogo de produto
     * @param c um objecto da classe Produto
     */
    public void add_produto(Produto c){
        String id;
        id=c.getId();
        cat_produto.put(id,c);
    }
    
    /**
     * Verifica se existe um produto no catálogo identificado por id
     * @param id string que identifica o Produto.
     * @return true se o catálogo contém um produto identificado por id,false caso contrário
     */
    public boolean existe_produto(String id){
        if(cat_produto.containsKey(id)){
            return true;
        }
        return false;
    }
    
    /**
     * Retorna o total de elementos no catálogo
     * @return tamanho do catálogo
     */
    public int get_size(){
        return cat_produto.size();
    }
    
    /**
     * Retorna o catálogo de produto
     * @return catálogo de produto
     */
    public HashMap<String, Produto> getCat_produto() {
        HashMap<String,Produto> aux=new HashMap<>();
        for(Produto c:cat_produto.values()){
            aux.put(c.getId(),c.clone());
        }
        return aux;
    }

    /**
     * Altera o valor do catálogo para o passado por argumento
     * @param cat_produto novos valores do catálogo
     */
    public void setCat_produto(HashMap<String, Produto> cat_produto) {
        HashMap<String,Produto> aux=new HashMap<>();
        for(Produto c:cat_produto.values()){
            aux.put(c.getId(),c.clone());
        }
        this.cat_produto = aux;
    }

    /**
     * Devolve a informação do catálogo sobre uma forma de texto
     * @return uma string com a informação do catálogo
     */
    public String toString() {
        StringBuilder s=new StringBuilder();
        for(Produto c:cat_produto.values()){
            s.append(c.toString());
            s.append(System.lineSeparator());
        }
        return s.toString(); 
    }
    
    /**
     * Cria um clone de objecto desta classe
     * @return o objecto clonado
     */
    public Cat_Produtos clone(){
        return new Cat_Produtos(this);
    }
    
    /**
     * Verifica se dois catálogos são iguais 
     * @param o catálogo a qual deve ser comparado
     * @return true se os catálogos foram iguais,false caso contrário
     */
    public boolean equals(Object o) {
       if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        return false;
    }
}