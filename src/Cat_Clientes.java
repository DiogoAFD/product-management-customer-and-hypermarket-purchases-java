import java.util.*;
import java.io.*;

/**
 *
 * @author Asus
 */

public class Cat_Clientes implements Serializable {
    
    private HashMap<String,Cliente> cat_cliente;
    
    /**
     * Construtor de classe com os valores por defeito
     */
    public Cat_Clientes(){
        cat_cliente=new HashMap<>();
    }
    
    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe 
     * @param c objecto com os valores
     */
    public Cat_Clientes(Cat_Clientes c){
        cat_cliente=c.getCat_cliente();
    }
    
    /**
     * Adiciona um cliente ao catálogo de clientes
     * @param c um objecto da classe Cliente
     */
    public void add_cliente(Cliente c){
        String id;
        id=c.getId();
        cat_cliente.put(id,c);
    }
    
    /**
     * Verifica se existe um cliente no catálogo identificado por id
     * @param id string que identifica o Cliente.
     * @return true se o catálogo contém um cliente identificado por id,false caso contrário
     */
    public boolean existe_cliente(String id){
        if(cat_cliente.containsKey(id)){
            return true;
        }
        return false;
    }
    
    /**
     * Retorna o total de elementos no catálogo
     * @return tamanho do catálogo
     */
    public int get_size(){
        return cat_cliente.size();
    }

    /**
     * Retorna o catálogo de clientes
     * @return catálogo de clientes
     */
    public HashMap<String, Cliente> getCat_cliente() {
        HashMap<String,Cliente> aux=new HashMap<>();
        for(Cliente c:cat_cliente.values()){
            aux.put(c.getId(),c.clone());
        }
        return aux;
    }

    /**
     * Altera o valor do catálogo para o passado por argumento
     * @param cat_cliente novos valores do catálogo
     */
    public void setCat_cliente(HashMap<String, Cliente> cat_cliente) {
        HashMap<String,Cliente> aux=new HashMap<>();
        for(Cliente c:cat_cliente.values()){
            aux.put(c.getId(),c.clone());
        }
        this.cat_cliente = aux;
    }

    /**
     * Devolve a informação do catálogo sobre uma forma de texto
     * @return uma string com a informação do catálogo
     */
    public String toString() {
        StringBuilder s=new StringBuilder();
        for(Cliente c:cat_cliente.values()){
            s.append(c.toString());
            s.append(System.lineSeparator());
        }
        return s.toString(); 
    }
    
    /**
     * Cria um clone de objecto desta classe
     * @return o objecto clonado
     */
    public Cat_Clientes clone(){
        return new Cat_Clientes(this);
    }
    
    /**
     * Verifica se dois catálogos são iguais 
     * @param o catálogo a qual deve ser comparado
     * @return true se os catálogos foram iguais,false caso contrário
     */
    public boolean equals(Object o) {
       if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        return false;
    }
    
    
}
