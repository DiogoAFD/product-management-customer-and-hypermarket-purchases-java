import java.util.*;
import java.io.*;

/**
 *
 * @author Miguel
 */
public class DoisArraysInt_DoisArraysFloats implements Serializable {

    private int[] a;
    private int[] b;
    private float[] c;
    private float[] d;
    
    /**
     * Construtor de classe com os valores por defeito
     */
    public DoisArraysInt_DoisArraysFloats() {
        a = new int[12];
        b = new int[12];
        c = new float[12];
        d = new float[12];
    }
    
     /**
     * Construtor de clasee com os valores passados por argumento
     * @param a   array de inteiros
     * @param b array de inteiros
     * @param c array de floats
     * @param d array de floats
     */
    public DoisArraysInt_DoisArraysFloats(int[] a, int[] b, float[] c, float[] d){
        this.a = a.clone();
        this.b = b.clone();
        this.c = c.clone();
        this.d = d.clone();
        
    }
    
    /**
     * Construtor de classe com os valores de um objecto dessa mesma classe 
     * @param d objecto com os valores
     */
    public DoisArraysInt_DoisArraysFloats(DoisArraysInt_DoisArraysFloats d) {
        this.a = d.get_A();
        this.b = d.get_B();
        this.c = d.get_C();
        this.d = d.get_D();
    }
    

    /**
     * Devolve um array de inteiros 
     * @return array de inteiros
     */
    public int[] get_A() {
        return a.clone();
    }

       /**
     * Devolve um array de inteiros 
     * @return array de inteiros
     */
    public int[] get_B() {
        return b.clone();
    }

       /**
     * Devolve um array de inteiros 
     * @return array de inteiros
     */
    public float[] get_C() {
        return c.clone();
    }

       /**
     * Devolve um array de inteiros 
     * @return array de inteiros
     */
    public float[] get_D() {
        return d.clone();
    }

      /**
     * Altera o valor do array a para o passado por argumento
     * @param a novo valor do array 
     */
    public void set_A(int[] a) {
        this.a = a.clone();
    }

    /**
     * Altera o valor do array a para o passado por argumento
     * @param b novo valor do array 
     */
    public void set_B(int[] b) {
        this.b = b.clone();
    }

    /**
     * Altera o valor do array a para o passado por argumento
     * @param c novo valor do array 
     */
    public void set_C(float[] c) {
        this.c = c.clone();
    }
    
    /**
     * Altera o valor do array a para o passado por argumento
     * @param d novo valor do array 
     */
    public void set_D(float[] d) {
        this.d = d.clone();
    }
    
    /**
     * Cria um clone de um objecto desta classe
     * @return o objecto clonado
     */
    public DoisArraysInt_DoisArraysFloats clone(){
        return new DoisArraysInt_DoisArraysFloats(this);
    }
    
    /**
     * Devolve a informação do objecto sobre a forma de texto
     * @return string com a informação
     */
    
   public String toString(){
        StringBuilder s = new StringBuilder();
        for(int i:a){
            s.append(a);
            s.append(System.lineSeparator());
        }
        for(int i:b){
            s.append(a);
            s.append(System.lineSeparator());
        }
        for(float f:c){
            s.append(a);
            s.append(System.lineSeparator());
        }
        
        for(float f:d){
            s.append(a);
            s.append(System.lineSeparator());
        }

        return s.toString();
    }
    
    /**
     * Verifica se dois objectos são iguais
     * @param o objecto a qual deve ser comparado
     * @return true se foram iguais, false caso contrário
     */
    public boolean equals(Object o) {
       if(o==this) return true;
        if((o==null)||(this.getClass()!=o.getClass())){
            return false;}
        DoisArraysInt_DoisArraysFloats a=(DoisArraysInt_DoisArraysFloats) o;
        return (this.a.equals(a.get_A()) && this.b.equals(a.get_B())&& this.c.equals(a.get_C())&& this.d.equals(a.get_D()));
    }

}
