import java.util.*;
import java.io.*;


public class CodigoExisteException extends Exception implements Serializable{
    public CodigoExisteException(){
        super();
    }
    
    public CodigoExisteException(String s){
        super(s);
    }
}
