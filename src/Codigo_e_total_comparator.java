
import java.util.Comparator;



/**
 *
 * @author Asus
 */
public class Codigo_e_total_comparator implements Comparator<Codigo_e_total> {
    
    @Override
    public int compare(Codigo_e_total o1, Codigo_e_total o2) {
        float tot1=o1.getTotal();
        float tot2=o2.getTotal();
        if(tot1==0){
            return 0;
        }
        if(tot1<tot2){
            return 1;
        }
        if(tot1>tot2){
            return -1;
        }
        if(tot2==tot1){
            return o1.getCodigo().compareTo(o2.getCodigo());
        }
        return 0;
    }
}
    

